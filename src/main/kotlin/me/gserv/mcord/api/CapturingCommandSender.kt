package me.gserv.mcord.api

import org.bukkit.Bukkit
import org.bukkit.Server
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.conversations.Conversation
import org.bukkit.conversations.ConversationAbandonedEvent
import org.bukkit.permissions.Permission
import org.bukkit.permissions.PermissionAttachment
import org.bukkit.permissions.PermissionAttachmentInfo
import org.bukkit.plugin.Plugin

class CapturingCommandSender : ConsoleCommandSender {
    private val bukkitSender = Bukkit.getConsoleSender()
    private val messageList: MutableList<String> = mutableListOf()

    val messages: List<String> get() = this.messageList.toList()

    override fun sendRawMessage(message: String) {
        this.messageList.add(message)
    }

    override fun sendMessage(message: String) {
        this.messageList.add(message)
    }

    override fun sendMessage(messages: Array<out String>) {
        this.messageList.addAll(messages)
    }

    override fun beginConversation(conversation: Conversation): Boolean {
        return this.bukkitSender.beginConversation(conversation)
    }

    override fun acceptConversationInput(input: String) {
        return this.bukkitSender.acceptConversationInput(input)
    }

    override fun isConversing(): Boolean {
        return this.bukkitSender.isConversing
    }

    override fun abandonConversation(conversation: Conversation) {
        return this.bukkitSender.abandonConversation(conversation)
    }

    override fun abandonConversation(conversation: Conversation, details: ConversationAbandonedEvent) {
        return this.bukkitSender.abandonConversation(conversation, details)
    }

    override fun spigot(): CommandSender.Spigot {
        return this.bukkitSender.spigot()
    }

    override fun isPermissionSet(name: String): Boolean {
        return this.bukkitSender.isPermissionSet(name)
    }

    override fun isPermissionSet(perm: Permission): Boolean {
        return this.bukkitSender.isPermissionSet(perm)
    }

    override fun addAttachment(plugin: Plugin, name: String, value: Boolean): PermissionAttachment {
        return this.bukkitSender.addAttachment(plugin, name, value)
    }

    override fun addAttachment(plugin: Plugin): PermissionAttachment {
        return this.bukkitSender.addAttachment(plugin)
    }

    override fun addAttachment(plugin: Plugin, name: String, value: Boolean, ticks: Int): PermissionAttachment? {
        return this.bukkitSender.addAttachment(plugin, name, value, ticks)
    }

    override fun addAttachment(plugin: Plugin, ticks: Int): PermissionAttachment? {
        return this.addAttachment(plugin, ticks)
    }

    override fun getName(): String {
        return this.bukkitSender.name
    }

    override fun isOp(): Boolean {
        return this.bukkitSender.isOp
    }

    override fun getEffectivePermissions(): MutableSet<PermissionAttachmentInfo> {
        return this.bukkitSender.effectivePermissions
    }

    override fun getServer(): Server {
        return this.bukkitSender.server
    }

    override fun removeAttachment(attachment: PermissionAttachment) {
        return this.bukkitSender.removeAttachment(attachment)
    }

    override fun recalculatePermissions() {
        return this.bukkitSender.recalculatePermissions()
    }

    override fun hasPermission(name: String): Boolean {
        return this.bukkitSender.hasPermission(name)
    }

    override fun hasPermission(perm: Permission): Boolean {
        return this.bukkitSender.hasPermission(perm)
    }

    override fun setOp(value: Boolean) {
        return this.bukkitSender.setOp(value)
    }

}