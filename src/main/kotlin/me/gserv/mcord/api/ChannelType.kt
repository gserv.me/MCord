package me.gserv.mcord.api

enum class ChannelType(val type: String) {
    ADMIN("admin"),
    RELAY("relay")
}
