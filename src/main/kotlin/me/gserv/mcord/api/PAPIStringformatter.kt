package me.gserv.mcord.api

import me.clip.placeholderapi.PlaceholderAPI.setPlaceholders
import me.gserv.mcord.Plugin
import org.bukkit.entity.Player

class PAPIStringformatter(plugin: Plugin) : StringFormatter(plugin) {
    override fun format(string: String, doEscapeDiscord: Boolean, message: String?, player: Player?, params: Map<String, String>?): String {
        return setPlaceholders(player, this.internalFormat(string, doEscapeDiscord, message, player, params))
    }
}
