package me.gserv.mcord.api

import com.google.gson.GsonBuilder
import discord4j.core.DiscordClient
import discord4j.core.`object`.entity.Category
import discord4j.core.`object`.entity.GuildMessageChannel
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.Webhook
import discord4j.core.`object`.util.Snowflake
import discord4j.core.spec.EmbedCreateSpec
import me.gserv.mcord.Plugin
import me.gserv.mcord.discord.events.BotCreatedEvent
import me.gserv.mcord.discord.events.BotDestroyedEvent
import me.gserv.mcord.discord.events.BotMessageEvent
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.netty.ByteBufFlux
import reactor.netty.http.client.HttpClient
import java.util.function.Consumer
import java.util.regex.Pattern

@Suppress("MemberVisibilityCanBePrivate")
class API(val plugin: Plugin) : Listener {
    val formatCodeRegex: Regex = Pattern.compile("\u00A7[0-9a-flmnopr]").toRegex()

    val bot: DiscordClient?
        get() {
            return plugin.client
        }

    var adminChannel: GuildMessageChannel? = null
    var relayChannel: GuildMessageChannel? = null
    var userCountChannel: Category? = null

    val formatter: StringFormatter = if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
        PAPIStringformatter(this.plugin)
    } else {
        StringFormatter(this.plugin)
    }

    fun isConnected(): Boolean {
        return this.bot != null && this.bot!!.isConnected
    }

    @EventHandler
    fun botCreated(event: BotCreatedEvent) {
        if (this.plugin.loadedConfig!!.adminChannel != null) {
            event.bot.getChannelById(Snowflake.of(this.plugin.loadedConfig!!.adminChannel!!)).subscribe {
                if (it is GuildMessageChannel) {
                    this.adminChannel = it
                    this.plugin.logger.info("Got admin channel: #${it.name}")
                } else {
                    this.plugin.logger.severe("Configured admin channel is not a text channel!")
                }
            }
        }

        if (this.plugin.loadedConfig!!.relayChannel != null) {
            event.bot.getChannelById(Snowflake.of(this.plugin.loadedConfig!!.relayChannel!!)).subscribe {
                if (it is GuildMessageChannel) {
                    this.relayChannel = it
                    this.plugin.logger.info("Got relay channel: #${it.name}")
                } else {
                    this.plugin.logger.severe("Configured relay channel is not a text channel!")
                }
            }
        }

        if (this.plugin.loadedConfig!!.userCountChannel != null) {
            event.bot.getChannelById(Snowflake.of(this.plugin.loadedConfig!!.userCountChannel!!)).subscribe {
                if (it is Category) {
                    this.userCountChannel = it
                    this.plugin.logger.info("Got user count channel: #${it.name}")

                    if (this.plugin.loadedConfig!!.formatDiscordUserCountChannel != null) {
                        this.userCountChannel!!.edit { channel ->
                            channel.setName(this.formatter.format(
                                    this.plugin.loadedConfig!!.formatDiscordUserCountChannel!!, true, null, null, mapOf("count" to Bukkit.getOnlinePlayers().size.toString())
                            ))
                        }.subscribe({}, { rename_error -> this.plugin.logger.severe("Unable to rename user count channel: $rename_error") })
                    }
                } else {
                    this.plugin.logger.severe("Configured user count channel is not on a server!")
                }
            }
        }
    }

    @EventHandler
    fun botDestroyed(event: BotDestroyedEvent) {
        this.adminChannel = null
        this.relayChannel = null
    }

    @EventHandler
    fun botMessage(event: BotMessageEvent) {
        if (
                this.plugin.loadedConfig?.formatMinecraftMessage == null ||
                event.event.message.channelId.asLong() != this.plugin.loadedConfig!!.relayChannel ||
                this.plugin.loadedConfig!!.commandPrefix?.let { event.cleanMessage.startsWith(it) } == true
        ) {
            return
        }

        Bukkit.broadcastMessage(
                this.formatter.format(
                        this.plugin.loadedConfig!!.formatMinecraftMessage!!,
                        false,
                        event.colouredMessage,
                        null,
                        mapOf(
                                "display_name" to event.authorDisplayName!!,
                                "username" to event.authorUserName!!,
                                "discriminator" to event.authorDiscriminator!!
                        )
                )
        )
    }

    fun sendBotMessage(channel: ChannelType, message: String? = null, embedCreateCallback: Consumer<EmbedCreateSpec>? = null, callback: Consumer<Message>? = null, errback: Consumer<in Throwable>? = null) {
        if (!this.isConnected()) {
            return
        }

        val guildChannel: GuildMessageChannel = (if (channel == ChannelType.ADMIN) {
            this.adminChannel
        } else {
            this.relayChannel
        }) ?: return

        val mono: Mono<Message> = guildChannel.createMessage {
            if (message != null) {
                it.setContent(message.replace(this.formatCodeRegex, ""))
            }

            if (embedCreateCallback != null) {
                it.setEmbed(embedCreateCallback)
            }
        }

        var callbackConsumer: Consumer<Message>? = callback
        var errbackConsumer: Consumer<in Throwable>? = errback

        if (callbackConsumer == null) {
            callbackConsumer = Consumer { }
        }

        errbackConsumer = if (errbackConsumer != null) {
            Consumer<Throwable> { error -> this.plugin.logger.severe("Failed to send message to ${channel.type} channel: $error") }.andThen(errbackConsumer)
        } else {
            Consumer { error -> this.plugin.logger.severe("Failed to send message to ${channel.type} channel: $error") }
        }

        mono.subscribe(callbackConsumer, errbackConsumer!!)
    }

    fun sendWebhookMessage(channel: ChannelType, message: String, player: Player? = null, author: String? = null, avatar_url: String? = null) {
        if (!this.isConnected()) {
            return
        }

        val guildChannel: GuildMessageChannel = (if (channel == ChannelType.ADMIN) {
            this.adminChannel
        } else {
            this.relayChannel
        }) ?: return  // If the channel is null, it wasn't configured, so there's nowhere to send to

        val webhook = this.ensureWebhook(guildChannel)

        if (webhook == null) {
            this.plugin.logger.severe("Failed to get webhook for the ${channel.type} channel - does the bot have permission to create one?")
            return
        }

        val data: MutableMap<String, String> = HashMap()
        data["content"] = message.replace(this.formatCodeRegex, "")

        if (player != null) {
            data["username"] = player.displayName.replace(this.formatCodeRegex, "")
            data["avatar_url"] = this.getAvatarUrl(player)
        } else {
            if (author != null) {
                data["username"] = author.replace(this.formatCodeRegex, "")
            }

            if (avatar_url != null) {
                data["avatar_url"] = avatar_url
            }
        }

        HttpClient.create()
                .followRedirect(true)
                .headers {
                    it.add("Content-Type", "application/json").add("User-Agent", "DiscordBot (https://gitlab.com/gserv.me/MCord, 0.1)")
                }
                .post()
                .uri("https://discordapp.com/api/webhooks/${webhook.id.asString()}/${webhook.token}")
                .send(ByteBufFlux.fromString(Flux.just(GsonBuilder().create().toJson(data))))
                .responseSingle { res, _ -> Mono.just(res.status().code()) }
                .subscribe(
                        { code -> if (code !in 200 until 300) this.plugin.logger.severe("Failed to submit webhook: HTTP response code was $code") },
                        { error -> this.plugin.logger.severe("Failed to submit webhook: $error") }
                )
    }

    private fun ensureWebhook(channel: GuildMessageChannel): Webhook? {
        var webhook: Webhook? = channel.webhooks.filter { hook -> hook.name.get() == "MCord" }.blockFirst()

        if (webhook == null) {
            webhook = channel.createWebhook { spec -> spec.setName("MCord") }.block()
        }

        return webhook
    }

    fun getAvatarUrl(player: Player): String {
        return "https://minotar.net/avatar/${player.uniqueId}.png"
    }
}
