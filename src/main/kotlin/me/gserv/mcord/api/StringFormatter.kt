package me.gserv.mcord.api

import me.gserv.mcord.Plugin
import org.bukkit.ChatColor.translateAlternateColorCodes
import org.bukkit.entity.Player
import java.lang.ref.WeakReference

open class StringFormatter(plugin: Plugin) {
    val pluginReference: WeakReference<Plugin> = WeakReference(plugin)

    open fun format(string: String, doEscapeDiscord: Boolean, message: String? = null, player: Player? = null, params: Map<String, String>? = null): String {
        return this.internalFormat(string, doEscapeDiscord, message, player, params)
    }

    protected fun internalFormat(string: String, doEscapeDiscord: Boolean, message: String? = null, player: Player? = null, params: Map<String, String>? = null): String {
        var formatted = string

        val escapeDiscord = if (doEscapeDiscord) {
            { str: String -> this.escapeDiscord(str) }
        } else {
            { str: String -> str }
        }

        if (message != null) {
            formatted = formatted.replace("{message}", message, true)
        }

        if (player != null) {
            formatted = formatted.replace("{display_name}", escapeDiscord(player.displayName), true)
            formatted = formatted.replace("{username}", escapeDiscord(player.name), true)
            formatted = formatted.replace("{uuid}", player.uniqueId.toString(), true)
        }

        if (params != null && params.isNotEmpty()) {
            for (key: String in params.keys) {
                formatted = formatted.replace("{$key}", escapeDiscord(params.getValue(key)), true)
            }
        }

        return translateAlternateColorCodes('&', formatted)
    }

    fun escapeDiscord(string: String): String {
        return string.replace("_", "\\_")
                .replace("*", "\\*")
                .replace("`", "\\`")
                .replace(">", "\\>")
    }
}
