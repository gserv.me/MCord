package me.gserv.mcord.config

import org.bukkit.configuration.file.FileConfiguration

class Config(private val config: FileConfiguration) {
    val token: String? get() = this.config.getString("bot.token")
    val commandPrefix: String? get() = this.config.getString("bot.command_prefix")

    val relayChannel: Long? get() = this.config.getString("channels.relay")?.toLong()
    val adminChannel: Long? get() = this.config.getString("channels.admin")?.toLong()
    val userCountChannel: Long? get() = this.config.getString("channels.user_count")?.toLong()

    val adminRole: Long? get() = this.config.getString("security.admin_role")?.toLong()

    val commandPermissionAnnounce: String? = this.config.getString("permissions.commands.announce")
    val commandPermissionAvatar: String? = this.config.getString("permissions.commands.avatar")
    val commandPermissionExec: String? = this.config.getString("permissions.commands.exec")
    val commandPermissionHelp: String? = this.config.getString("permissions.commands.help")
    val commandPermissionList: String? = this.config.getString("permissions.commands.list")

    val relayPermission: String? = this.config.getString("permissions.relay")

    var aliases: Map<String, String> = HashMap()

    init {
        val aliasesSection = this.config.getConfigurationSection("aliases")

        if (aliasesSection != null) {
            val aliasesMap = mutableMapOf<String, String>()

            for (key in aliasesSection.getKeys(false)) {
                aliasesMap[key] = aliasesSection.getString(key)!!
            }

            aliases = aliasesMap.toMap()
        }
    }

    val formatMinecraftAnnounce: String? = this.config.getString("formatting.minecraft.announce")
    val formatMinecraftMessage: String? = this.config.getString("formatting.minecraft.message")

    val formatMinecraftUserMentionPrefix: String = this.config.getString("formatting.minecraft.user_mention_prefix")
            ?: ""
    val formatMinecraftUserMentionSuffix: String = this.config.getString("formatting.minecraft.user_mention_suffix")
            ?: ""
    val formatMinecraftRoleMentionPrefix: String = this.config.getString("formatting.minecraft.role_mention_prefix")
            ?: ""
    val formatMinecraftRoleMentionSuffix: String = this.config.getString("formatting.minecraft.role_mention_suffix")
            ?: ""
    val formatMinecraftChannelMentionPrefix: String = this.config.getString("formatting.minecraft.channel_mention_prefix")
            ?: ""
    val formatMinecraftChannelMentionSuffix: String = this.config.getString("formatting.minecraft.channel_mention_suffix")
            ?: ""

    val formatDiscordUserCountChannel: String? = this.config.getString("formatting.discord.user_count_channel")

    val formatDiscordWebhookUse: Boolean = this.config.getBoolean("formatting.discord.webhook.use")
    val formatDiscordWebhookUseAvatar: Boolean = this.config.getBoolean("formatting.discord.webhook.use_player_avatar")
    val formatDiscordWebhookUsePlayerName: Boolean = this.config.getBoolean("formatting.discord.webhook.use_player_name")

    val formatDiscordWebhookCustomSenderName: String? = this.config.getString("formatting.discord.webhook.custom_sender_name")
    val formatDiscordWebhookCustomSenderAvatar: String? = this.config.getString("formatting.discord.webhook.custom_sender_avatar")

    val formatDiscordWebhookServerSenderName: String? = this.config.getString("formatting.discord.webhook.server_sender_name")
    val formatDiscordWebhookServerSenderAvatar: String? = this.config.getString("formatting.discord.webhook.server_sender_avatar")

    val formatDiscordMessageAdvancement: String? = this.config.getString("formatting.discord.messages.advancement")
    val formatDiscordMessageBan: String? = this.config.getString("formatting.discord.messages.ban")
    val formatDiscordMessageDeath: String? = this.config.getString("formatting.discord.messages.death")
    val formatDiscordMessageJoin: String? = this.config.getString("formatting.discord.messages.join")
    val formatDiscordMessageKick: String? = this.config.getString("formatting.discord.messages.kick")
    val formatDiscordMessageQuit: String? = this.config.getString("formatting.discord.messages.quit")


    val formatDiscordMessageMessage: String? = this.config.getString("formatting.discord.messages.message")

    val formatDiscordLoggingEmbed: Boolean = this.config.getBoolean("formatting.discord.logging.use_embed")
    val formatDiscordLoggingCommand: String? = this.config.getString("formatting.discord.logging.messages.command_used")
    val formatDiscordLoggingSign: String? = this.config.getString("formatting.discord.logging.messages.sign_placed")
}
