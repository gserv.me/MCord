package me.gserv.mcord.minecraft.listeners

import me.gserv.mcord.Plugin
import me.gserv.mcord.api.ChannelType
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent
import java.lang.ref.WeakReference

class QuitListener(plugin: Plugin) : Listener {
    private val pluginRef: WeakReference<Plugin> = WeakReference(plugin)
    private val api get() = this.pluginRef.get()?.api

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val message: String? = this.pluginRef.get()!!.loadedConfig?.formatDiscordMessageQuit?.let { this.api?.formatter?.format(it, true, null, event.player) }

        if (message != null) {
            this.api?.sendBotMessage(ChannelType.RELAY, message)
        }

        if (this.pluginRef.get()!!.loadedConfig!!.formatDiscordUserCountChannel != null) {
            this.pluginRef.get()!!.api.userCountChannel!!.edit { channel ->
                channel.setName(this.pluginRef.get()!!.api.formatter.format(
                        this.pluginRef.get()!!.loadedConfig!!.formatDiscordUserCountChannel!!, true, null, null,
                        mapOf("count" to (Bukkit.getOnlinePlayers().size - 1).toString())
                ))
            }.subscribe({}, { rename_error -> this.pluginRef.get()!!.logger.severe("Unable to rename user count channel: $rename_error") })
        }
    }
}
