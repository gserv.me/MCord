package me.gserv.mcord.minecraft.listeners

import me.gserv.mcord.Plugin
import me.gserv.mcord.api.ChannelType
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import java.lang.ref.WeakReference

class DeathListener(plugin: Plugin) : Listener {
    private val pluginRef: WeakReference<Plugin> = WeakReference(plugin)
    private val api get() = this.pluginRef.get()?.api

    @EventHandler
    fun onPlayerDeath(event: PlayerDeathEvent) {
        var deathMessage = event.deathMessage

        if (deathMessage?.startsWith(event.entity.displayName, false) == true) {
            deathMessage = deathMessage.substring(event.entity.displayName.length + 1)
        }

        val message: String? = this.pluginRef.get()!!.loadedConfig?.formatDiscordMessageDeath?.let { this.api?.formatter?.format(it, true, deathMessage, event.entity) }

        if (message != null) {
            this.api?.sendBotMessage(ChannelType.RELAY, message)
        }
    }
}
