package me.gserv.mcord.minecraft.listeners

import me.gserv.mcord.Plugin
import me.gserv.mcord.api.ChannelType
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.SignChangeEvent
import java.awt.Color
import java.lang.ref.WeakReference
import java.time.Instant
import java.util.function.Consumer

class SignListener(plugin: Plugin) : Listener {
    private val pluginRef: WeakReference<Plugin> = WeakReference(plugin)
    private val api get() = this.pluginRef.get()?.api

    @EventHandler
    fun onSignEdit(event: SignChangeEvent) {
        val data = mapOf(
                "line_1" to (event.getLine(0) ?: ""),
                "line_2" to (event.getLine(1) ?: ""),
                "line_3" to (event.getLine(2) ?: ""),
                "line_4" to (event.getLine(3) ?: "")
        )

        val message: String? = this.pluginRef.get()!!.loadedConfig?.formatDiscordLoggingSign?.let { this.api?.formatter?.format(it, true, null, event.player, data) }

        if (message != null && this.api != null) {
            this.api?.sendBotMessage(ChannelType.ADMIN, null, Consumer { embed ->
                embed.setTitle("Sign edited")
                embed.setAuthor(event.player.displayName.replace(this.api!!.formatCodeRegex, ""), null, this.api!!.getAvatarUrl(event.player))
                embed.setDescription(message)
                embed.setTimestamp(Instant.now())
                embed.setColor(Color.ORANGE)
            })
        }
    }
}
