package me.gserv.mcord.minecraft.listeners

import me.gserv.mcord.Plugin
import me.gserv.mcord.api.ChannelType
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerKickEvent
import java.lang.ref.WeakReference

class KickListener(plugin: Plugin) : Listener {
    private val pluginRef: WeakReference<Plugin> = WeakReference(plugin)
    private val api get() = this.pluginRef.get()?.api

    @EventHandler
    fun onPlayerKick(event: PlayerKickEvent) {
        val message: String? = this.pluginRef.get()!!.loadedConfig?.formatDiscordMessageKick?.let {
            this.api?.formatter?.format(it, true, null, event.player, mapOf("reason" to event.reason))
        }

        if (message != null) {
            this.api?.sendBotMessage(ChannelType.RELAY, message)
        }
    }
}
