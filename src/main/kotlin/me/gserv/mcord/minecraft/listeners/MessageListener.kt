package me.gserv.mcord.minecraft.listeners

import me.gserv.mcord.Plugin
import me.gserv.mcord.api.ChannelType
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import java.lang.ref.WeakReference

class MessageListener(plugin: Plugin) : Listener {
    private val pluginRef: WeakReference<Plugin> = WeakReference(plugin)
    private val api get() = this.pluginRef.get()?.api

    @EventHandler
    fun onChat(event: AsyncPlayerChatEvent) {
        val message: String? = this.pluginRef.get()!!.loadedConfig?.formatDiscordMessageMessage?.let { this.api?.formatter?.format(it, true, event.message, event.player) }

        if (message != null) {
            this.pluginRef.get()!!.api.sendWebhookMessage(
                    ChannelType.RELAY,
                    message,
                    event.player
            )
        }
    }
}
