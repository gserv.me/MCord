package me.gserv.mcord.discord

import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.util.Image
import discord4j.core.`object`.util.Snowflake
import discord4j.core.spec.EmbedCreateSpec
import me.gserv.mcord.Plugin
import me.gserv.mcord.api.CapturingCommandSender
import me.gserv.mcord.api.ChannelType
import me.gserv.mcord.discord.events.BotMessageEvent
import org.apache.commons.lang.text.StrTokenizer
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import java.awt.Color
import java.time.Instant
import java.util.function.Consumer

class CommandHandler(private val plugin: Plugin, private val dispatcher: EventDispatcher) : Listener {
    @EventHandler
    fun processEvent(event: BotMessageEvent) {
        if (event.message == null) {
            return
        }

        this.plugin.logger.info("${event.authorUserName}#${event.authorDiscriminator} -> ${event.cleanMessage}")

        val parts: List<String> = StrTokenizer(event.cleanMessage).tokenArray.asList()

        if (parts.isEmpty()) {
            return
        }

        if (!this.plugin.loadedConfig?.commandPrefix?.let { parts[0].startsWith(it) }!!) {
            return
        }

        val command: String = parts[0].substring(this.plugin.loadedConfig!!.commandPrefix!!.length).toLowerCase()

        val args: List<String>? = if (parts.size > 1) {
            parts.slice(1 until parts.size).onEach { str -> str.trim('\'', '"') }
        } else {
            null
        }

        val argString = if (!args.isNullOrEmpty()) {
            event.cleanMessage.substring(this.plugin.loadedConfig!!.commandPrefix!!.length + command.length + 1)
        } else {
            ""
        }

        when (command) {
            "announce" -> if (canRunCommand(command, event.event.member.get())) this.announceCommand(args, argString, event)
            "avatar" -> if (canRunCommand(command, event.event.member.get())) this.avatarCommand(args, argString, event)
            "exec" -> if (canRunCommand(command, event.event.member.get())) this.execCommand(args, argString, event)
            "help" -> if (canRunCommand(command, event.event.member.get())) this.helpCommand(args, argString, event)
            "list" -> if (canRunCommand(command, event.event.member.get())) this.listCommand(args, argString, event)

            else -> this.handleAlias(command, args, argString, event)
        }
    }

    private fun announceCommand(args: List<String>?, argString: String, event: BotMessageEvent) {
        val channelType = this.getChannelType(event) ?: return

        if (args.isNullOrEmpty()) {
            this.plugin.api.sendBotMessage(
                    channelType,
                    "${event.event.member.get().mention} Usage: **${this.plugin.loadedConfig!!.commandPrefix!!}announce <message>**"
            )

            return
        }

        val announcementString = this.plugin.loadedConfig?.formatMinecraftAnnounce

        if (announcementString == null) {
            this.plugin.api.sendBotMessage(
                    channelType,
                    "${event.event.member.get().mention} **Unable to send announcement:** Please configure the `formatting.minecraft.announce` option in the configuration file."
            )

            return
        }

        Bukkit.broadcastMessage(this.plugin.api.formatter.format(announcementString, false, argString))

        this.plugin.api.sendBotMessage(
                channelType,
                "${event.event.member.get().mention} Announcement broadcast successfully."
        )
    }

    private fun avatarCommand(args: List<String>?, argString: String, event: BotMessageEvent) {
        val channelType = this.getChannelType(event) ?: return

        if (args.isNullOrEmpty()) {
            this.plugin.api.sendBotMessage(
                    channelType,
                    "${event.event.member.get().mention} Usage: **${this.plugin.loadedConfig!!.commandPrefix!!}avatar <url>**"
            )

            return
        }

        var url = args[0]

        if (url.startsWith("<") && url.endsWith(">")) {
            url = url.substring(1 until url.length - 1)
        }

        Image.ofUrl(url).subscribe(
                { image ->
                    this.plugin.client!!.edit { it.setAvatar(image) }.doOnError { error ->
                        this.plugin.api.sendBotMessage(
                                channelType,
                                "${event.event.member.get().mention} Failed to set avatar.\n" +
                                        "> `${error.localizedMessage}"
                        )
                    }.doOnSuccess {
                        this.plugin.api.sendBotMessage(
                                channelType,
                                "${event.event.member.get().mention} Avatar set successfully."
                        )
                    }.subscribe()
                }, { error ->
            this.plugin.api.sendBotMessage(
                    channelType,
                    "${event.event.member.get().mention} Failed to set avatar.\n" +
                            "> `${error.localizedMessage}`"
            )
        }
        )
    }

    private fun execCommand(args: List<String>?, argString: String, event: BotMessageEvent) {
        val channelType = this.getChannelType(event) ?: return

        if (args.isNullOrEmpty()) {
            this.plugin.api.sendBotMessage(
                    channelType,
                    "${event.event.member.get().mention} Usage: **${this.plugin.loadedConfig!!.commandPrefix!!}exec <command>**"
            )

            return
        }

        val command = args.joinToString(" ")

        val sender = CapturingCommandSender()
        Bukkit.dispatchCommand(sender, command)

        var message = "${event.event.member.get().mention} Command has been run."

        if (sender.messages.isNotEmpty()) {
            message += " Output:\n>>> \n```\n"
            for (commandMessage in sender.messages) {
                message += commandMessage
            }

            message += "```"
        }

        this.plugin.api.sendBotMessage(channelType, "${event.event.member.get().mention} Command has been run.")
    }

    private fun helpCommand(args: List<String>?, argString: String, event: BotMessageEvent) {
        val channelType = this.getChannelType(event) ?: return

        val prefix = this.plugin.loadedConfig!!.commandPrefix

        var commands = ""

        commands += "**${prefix}announce <message>** Send an announcement to all online players\n"
        commands += "**${prefix}avatar <url>** Set the bot's avatar\n"
        commands += "**${prefix}exec <command>** Run a command at the server console\n"
        commands += "**${prefix}help** Show this help message\n"
        commands += "**${prefix}list** Show the online players\n"

        var aliases = ""

        for (alias in this.plugin.loadedConfig!!.aliases.entries) {
            aliases += "**${prefix}${alias.key}** -> `/${alias.value}`\n"
        }

        this.plugin.api.sendBotMessage(channelType, null, Consumer { embed ->
            embed.setTitle("Help")
            embed.addField("Commands", commands, false)

            if (aliases.isNotEmpty()) {
                embed.addField("Server Command Aliases", aliases, false)
            } else {
                embed.addField("Aliases", "No command aliases configured.", false)
            }
        })
    }

    private fun listCommand(args: List<String>?, argString: String, event: BotMessageEvent) {
        val channelType = this.getChannelType(event) ?: return

        val players = Bukkit.getOnlinePlayers()
        var list = ""

        if (!players.isEmpty()) {
            for (player in players) {
                list += "**>** ${player.name}\n"
            }
        } else {
            list = "**No players online**"
        }

        this.plugin.api.sendBotMessage(channelType, null, Consumer { embed: EmbedCreateSpec ->
            embed.setTitle("Online Players (${players.size})")
            embed.setColor(Color.GREEN)
            embed.setDescription(list)
            embed.setTimestamp(Instant.now())
        })
    }

    private fun handleAlias(name: String, args: List<String>?, argString: String, event: BotMessageEvent) {
        val channelType = this.getChannelType(event) ?: return
        val role = this.plugin.loadedConfig?.adminRole

        if (role != null && this.hasRole(role, event.event.member.get())) {
            val command = this.plugin.loadedConfig!!.aliases.getOrDefault(name, "")

            if (command.isEmpty()) {
                return
            }

            val sender = CapturingCommandSender()
            Bukkit.dispatchCommand(sender, "$command $argString")

            var message = "${event.event.member.get().mention} Command has been run."

            if (sender.messages.isNotEmpty()) {
                message += " Output:\n>>> \n```\n"
                for (commandMessage in sender.messages) {
                    message += commandMessage
                }

                message += "```"
            }

            this.plugin.api.sendBotMessage(channelType, "${event.event.member.get().mention} Command has been run.")
        }
    }

    private fun getChannelType(event: BotMessageEvent): ChannelType? {
        val channelId = event.event.message.channelId

        return when {
            channelId.asLong() == this.plugin.loadedConfig!!.adminChannel -> ChannelType.ADMIN
            channelId.asLong() == this.plugin.loadedConfig!!.relayChannel -> ChannelType.RELAY

            else -> null
        }
    }

    private fun canRunCommand(command: String, member: Member): Boolean {
        val permission: String = when (command) {
            "announce" -> this.plugin.loadedConfig?.commandPermissionAnnounce
            "avatar" -> this.plugin.loadedConfig?.commandPermissionAvatar
            "exec" -> this.plugin.loadedConfig?.commandPermissionExec
            "help" -> this.plugin.loadedConfig?.commandPermissionHelp
            "list" -> this.plugin.loadedConfig?.commandPermissionList

            else -> null
        } ?: return false

        if (permission.toLowerCase() == "user") {
            return true
        } else if (permission.toLowerCase() == "admin") {
            val role = this.plugin.loadedConfig?.adminRole

            if (role != null) {
                return this.hasRole(role, member)
            }
        }

        return false
    }

    private fun hasRole(role: Long, member: Member): Boolean {
        return member.roleIds.contains(Snowflake.of(role))
    }
}