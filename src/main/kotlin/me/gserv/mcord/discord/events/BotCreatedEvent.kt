package me.gserv.mcord.discord.events

import discord4j.core.DiscordClient
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class BotCreatedEvent(val bot: DiscordClient) : Event(false) {
    companion object {  // Bukkit requires the below method to be static for whatever reason
        @JvmStatic
        private val handlers: HandlerList = HandlerList()

        @JvmStatic
        fun getHandlerList(): HandlerList {
            return this.handlers
        }
    }

    override fun getHandlers(): HandlerList {
        return getHandlerList()
    }
}
