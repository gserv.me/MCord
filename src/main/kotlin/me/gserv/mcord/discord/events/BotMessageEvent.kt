package me.gserv.mcord.discord.events

import discord4j.core.`object`.entity.Member
import discord4j.core.event.domain.message.MessageCreateEvent
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class BotMessageEvent(private val discordEvent: MessageCreateEvent, private val cleanContent: String, private val colouredContent: String) : Event() {
    companion object {  // Bukkit requires the below method to be static for whatever reason
        @JvmStatic
        private val handlers: HandlerList = HandlerList()

        @JvmStatic
        fun getHandlerList(): HandlerList {
            return this.handlers
        }
    }

    val event: MessageCreateEvent get() = this.discordEvent

    val message: String?
        get() {
            if (this.discordEvent.message.content.isPresent) {
                return this.discordEvent.message.content.get()
            }

            return null
        }

    val cleanMessage: String
        get() {
            return this.cleanContent
        }

    val colouredMessage: String
        get() {
            return this.colouredContent
        }

    val authorDisplayName: String?
        get() {
            return if (this.discordEvent.message.author.isPresent) {
                if (this.discordEvent.message.author.get() is Member) {
                    (this.discordEvent.message.author.get() as Member).displayName
                } else {
                    this.discordEvent.message.author.get().username
                }
            } else {
                null
            }
        }

    val authorNickName: String?
        get() {
            return if (this.discordEvent.message.author.isPresent) {
                if (this.discordEvent.message.author.get() is Member) {
                    if ((this.discordEvent.message.author.get() as Member).nickname.isPresent) {
                        (this.discordEvent.message.author.get() as Member).nickname.get()
                    } else {
                        null
                    }
                } else {
                    this.discordEvent.message.author.get().username
                }
            } else {
                null
            }
        }

    val authorUserName: String?
        get() {
            return if (this.discordEvent.message.author.isPresent) {
                this.discordEvent.message.author.get().username
            } else {
                null
            }
        }

    val authorDiscriminator: String?
        get() {
            return if (this.discordEvent.message.author.isPresent) {
                this.discordEvent.message.author.get().discriminator
            } else {
                null
            }
        }

    override fun getHandlers(): HandlerList {
        return getHandlerList()
    }
}