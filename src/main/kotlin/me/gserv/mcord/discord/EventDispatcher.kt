package me.gserv.mcord.discord

import discord4j.core.DiscordClient
import discord4j.core.`object`.entity.Channel
import discord4j.core.`object`.entity.GuildChannel
import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.domain.message.MessageCreateEvent
import me.gserv.mcord.Plugin
import me.gserv.mcord.discord.events.BotCreatedEvent
import me.gserv.mcord.discord.events.BotDestroyedEvent
import me.gserv.mcord.discord.events.BotMessageEvent
import org.bukkit.Bukkit
import org.bukkit.event.Event
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.regex.Matcher
import java.util.regex.Pattern

class EventDispatcher(private val plugin: Plugin) {
    private val channelRegex: Pattern = Pattern.compile("<#(\\d+)>")

    fun botCreated(bot: DiscordClient) {
        this.callEvent(BotCreatedEvent(bot))
    }

    fun botDestroyed() {
        this.callEvent(BotDestroyedEvent())
    }

    fun setupBotEvents() {
        this.plugin.client!!.eventDispatcher.on(MessageCreateEvent::class.java).subscribe { event ->
            if (event.message.type == Message.Type.DEFAULT &&
                    event.message.author.isPresent &&
                    event.message.author.get().id != this.plugin.client!!.selfId.get() && (
                            event.message.channelId.asLong() == this.plugin.loadedConfig!!.relayChannel ||
                                    event.message.channelId.asLong() == this.plugin.loadedConfig!!.adminChannel
                            )
            ) {

                if (event.message.content.isPresent) {
                    var content: String = event.message.content.get()
                    // TODO: Colours
                    var colouredContent: String = event.message.content.get()

                    event.message.roleMentions.collectList().subscribe { roles ->
                        for (role in roles) {
                            content = content.replace("<@&${role.id.asString()}>", "@${role.name}")
                            colouredContent = colouredContent.replace(
                                    "<@&${role.id.asString()}>",
                                    "${this.plugin.loadedConfig!!.formatMinecraftRoleMentionPrefix}@${role.name}${this.plugin.loadedConfig!!.formatMinecraftRoleMentionSuffix}"
                            )
                        }

                        event.message.userMentions.collectList().subscribe { users ->
                            for (user in users) {
                                if (user is Member) {
                                    content = content.replace("<@${user.id.asString()}>", "@${user.displayName}")
                                    content = content.replace("<@!${user.id.asString()}>", "@${user.displayName}")
                                    colouredContent = colouredContent.replace(
                                            "<@${user.id.asString()}>",
                                            "${this.plugin.loadedConfig!!.formatMinecraftUserMentionPrefix}@${user.displayName}${this.plugin.loadedConfig!!.formatMinecraftUserMentionSuffix}"
                                    )
                                    colouredContent = colouredContent.replace(
                                            "<@!${user.id.asString()}>",
                                            "${this.plugin.loadedConfig!!.formatMinecraftUserMentionPrefix}@${user.displayName}${this.plugin.loadedConfig!!.formatMinecraftUserMentionSuffix}"
                                    )
                                } else {
                                    content = content.replace("<@${user.id.asString()}>", "@${user.username}")
                                    content = content.replace("<@!${user.id.asString()}>", "@${user.username}")
                                    colouredContent = colouredContent.replace(
                                            "<@${user.id.asString()}>",
                                            "${this.plugin.loadedConfig!!.formatMinecraftUserMentionPrefix}@${user.username}${this.plugin.loadedConfig!!.formatMinecraftUserMentionSuffix}"
                                    )
                                    colouredContent = colouredContent.replace(
                                            "<@!${user.id.asString()}>",
                                            "${this.plugin.loadedConfig!!.formatMinecraftUserMentionPrefix}@${user.username}${this.plugin.loadedConfig!!.formatMinecraftUserMentionSuffix}"
                                    )
                                }
                            }

                            val matcher: Matcher = this.channelRegex.matcher(content)
                            val matches: MutableList<Snowflake> = mutableListOf()

                            while (matcher.find()) {
                                matches.add(Snowflake.of(matcher.group(1)))
                            }

                            val channelFlux: Flux<Channel> = Flux.fromIterable<Snowflake>(matches).flatMap<Channel> { channelId ->
                                this.plugin.client!!.getChannelById(channelId).onErrorResume { Mono.just(FakeChannel(channelId)) }
                            }

                            channelFlux.collectList().subscribe(
                                    { channels ->
                                        for (channel in channels) {
                                            channel as GuildChannel

                                            content = content.replace("<#${channel.id.asString()}>", "#${channel.name}")

                                            colouredContent = colouredContent.replace(
                                                    "<#${channel.id.asString()}>",
                                                    "${this.plugin.loadedConfig!!.formatMinecraftChannelMentionPrefix}#${channel.name}${this.plugin.loadedConfig!!.formatMinecraftChannelMentionSuffix}"
                                            )
                                        }

                                        this.callEvent(BotMessageEvent(event, content, colouredContent))
                                    },
                                    { this.plugin.logger.info("Failed to get all channels from message.") }
                            )
                        }
                    }
                }
            }
        }
    }

    private fun callEvent(event: Event) {
        if (Thread.currentThread().id != this.plugin.threadId) {
            Bukkit.getScheduler().runTask(this.plugin) { -> Bukkit.getPluginManager().callEvent(event) }
        } else {
            Bukkit.getPluginManager().callEvent(event)
        }
    }
}
