package me.gserv.mcord.discord

import discord4j.core.DiscordClient
import discord4j.core.`object`.ExtendedPermissionOverwrite
import discord4j.core.`object`.PermissionOverwrite
import discord4j.core.`object`.entity.Channel
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.GuildChannel
import discord4j.core.`object`.util.PermissionSet
import discord4j.core.`object`.util.Snowflake
import reactor.core.publisher.Mono
import java.util.*

class FakeChannel(private val _id: Snowflake) : GuildChannel {
    override fun addRoleOverwrite(roleId: Snowflake, overwrite: PermissionOverwrite, reason: String?): Mono<Void> {
        TODO("not implemented")
    }

    override fun getName(): String {
        return "deleted-channel"
    }

    override fun addMemberOverwrite(memberId: Snowflake, overwrite: PermissionOverwrite, reason: String?): Mono<Void> {
        TODO("not implemented")
    }

    override fun getRawPosition(): Int {
        TODO("not implemented")
    }

    override fun getPosition(): Mono<Int> {
        TODO("not implemented")
    }

    override fun getGuildId(): Snowflake {
        TODO("not implemented")
    }

    override fun getEffectivePermissions(memberId: Snowflake): Mono<PermissionSet> {
        TODO("not implemented")
    }

    override fun getOverwriteForRole(roleId: Snowflake): Optional<ExtendedPermissionOverwrite> {
        TODO("not implemented")
    }

    override fun getOverwriteForMember(memberId: Snowflake): Optional<ExtendedPermissionOverwrite> {
        TODO("not implemented")
    }

    override fun getGuild(): Mono<Guild> {
        TODO("not implemented")
    }

    override fun getPermissionOverwrites(): MutableSet<ExtendedPermissionOverwrite> {
        TODO("not implemented")
    }

    override fun getClient(): DiscordClient {
        TODO("not implemented")
    }

    override fun getId(): Snowflake {
        return this._id
    }

    override fun getType(): Channel.Type {
        TODO("not implemented")
    }

    override fun delete(reason: String?): Mono<Void> {
        TODO("not implemented")
    }
}