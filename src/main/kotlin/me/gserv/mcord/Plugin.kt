package me.gserv.mcord

import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.event.domain.lifecycle.ReadyEvent
import me.gserv.mcord.api.API
import me.gserv.mcord.config.Config
import me.gserv.mcord.discord.CommandHandler
import me.gserv.mcord.discord.EventDispatcher
import me.gserv.mcord.minecraft.listeners.*
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import kotlin.concurrent.thread


class Plugin : JavaPlugin() {
    var client: DiscordClient? = null
    var loadedConfig: Config? = null

    private var isLoaded: Boolean = System.getProperty("MCORE_LOADED", "") == "LOADED"

    private val _threadId: Long = Thread.currentThread().id
    val threadId: Long get() = this._threadId

    private val _api: API = API(this)
    val api: API get() = this._api

    private val eventDispatcher = EventDispatcher(this)
    private val commandHandler = CommandHandler(this, this.eventDispatcher)

    override fun onEnable() {
        super.onEnable()

        if (this.isLoaded) {
            this.logger.severe("This plugin does not support direct reloading - please restart your server!")
            Bukkit.getPluginManager().disablePlugin(this)
            return
        }

        System.setProperty("MCORE_LOADED", "LOADED")

        this.saveDefaultConfig()
        this.loadConfig()

        this.server.pluginManager.registerEvents(this.api, this)
        this.server.pluginManager.registerEvents(this.commandHandler, this)

        this.server.pluginManager.registerEvents(CommandListener(this), this)
        this.server.pluginManager.registerEvents(DeathListener(this), this)
        this.server.pluginManager.registerEvents(JoinListener(this), this)
        this.server.pluginManager.registerEvents(KickListener(this), this)
        this.server.pluginManager.registerEvents(MessageListener(this), this)
        this.server.pluginManager.registerEvents(QuitListener(this), this)
        this.server.pluginManager.registerEvents(SignListener(this), this)

        this.setupClient()
    }

    override fun onDisable() {
        super.onDisable()

        if (this.client != null) {
            this.client!!.logout().block()
            eventDispatcher.botDestroyed()

            this.client = null
        }
    }

    private fun loadConfig() {
        this.loadedConfig = Config(this.config)
    }

    private fun setupClient() {
        if (this.loadedConfig == null || this.loadedConfig!!.token == null || this.loadedConfig!!.token!!.isEmpty()) {
            this.logger.warning("No token found in the configuration: Please set up the plugin!")
            return
        }

        if (this.client != null) {
            this.client!!.logout().subscribe {
                eventDispatcher.botDestroyed()

                this.client = null
                this.loginClient()
            }
        } else {
            this.loginClient()
        }
    }

    private fun loginClient() {
        this.logger.info("Connecting to Discord..")

        this.client = DiscordClientBuilder(this.loadedConfig!!.token!!).build()
        this.client!!.eventDispatcher.on(ReadyEvent::class.java).subscribe { this.eventDispatcher.botCreated(this.client!!) }
        this.eventDispatcher.setupBotEvents()

        // TODO: Set up events and stuff

        thread {
            this.client!!.login().block()
        }
    }
}
