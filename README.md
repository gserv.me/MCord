# MCord

**MCord** is more or less my attempt at solving the age-old problem of getting your Minecraft server connected to Discord.
The first version is probably spaghetti code - I'm using this to learn Kotlin. It does, however, contain a few features that
I couldn't find anywhere else (for example, sending messages using webhooks).

That said, this plugin is in the very early stages, and is just a concept right now. It more or less work, but it'll need
a major cleanup and the addition of quite a lot of features before it's ready for general release.

---

## Project Status

This project is still in the early stages, and it's not ready to be used just yet.

## Requirements

* [KotlinPlugin](https://www.spigotmc.org/resources/kotlinplugin-allow-to-use-kotlin-corountines-in-your-plugins.70526/)
* Optionally: [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/)

## Building the project

This project makes use of **Gradle**. Open a terminal and run the following commands from the project directory:

* **Windows**: `gradlew jar`
* **Linux/Mac**: `./gradlew jar`

You will require a JDK installed for this - version 8 or later.

Once this task has been run, you will find a JAR file in `build/lib`, named `PagedCPluginhests-<version>.jar`. This is the
plugin - drop it in your `plugins/` folder to get started.

## Questions & Contributions

Questions, concerns, ideas? [Open an issue!](https://gitlab.com/gserv.me/MCord/issues/new) Pull requests are
welcome, once the project has gotten off the ground.
